'use strict'

const { sanitizeEntity } = require('strapi-utils')

const getOrderEntry = async (id) => {
  const orderEntry = await strapi.services['order-entry'].findOne({ id })
  return sanitizeEntity(orderEntry, { model: strapi.models['order-entry'] })
}

const getTotalPrice = async (orderEntriesIds) => {
  try {
    if (orderEntriesIds) {
      const orderEntries = await Promise.all(orderEntriesIds.map(entryId => getOrderEntry(entryId)))
      return orderEntries.reduce((accumulator, currentValue) => accumulator + currentValue.price, 0)
    }
  } catch (e) {
    console.log('Cannot calculate price for order, something wrong with entries', e)
  }
}

module.exports = {
  lifecycles: {
    beforeCreate: async (data) => {
      data.totalPrice = await getTotalPrice(data.orderEntries)
    },
    beforeUpdate: async (params, data) => {
      data.totalPrice = await getTotalPrice(data.orderEntries)
    }
  }
}
