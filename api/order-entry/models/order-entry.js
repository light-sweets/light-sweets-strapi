'use strict'

const { sanitizeEntity } = require('strapi-utils')

const updateOrder = async id => {
  try {
    const order = await strapi.services.order.findOne({ id })
    const sanitizedOrder = sanitizeEntity(order, { model: strapi.models.order })
    const orderEntries = sanitizedOrder.orderEntries.map(entry => entry.id)
    strapi.services.order.update({ id }, { orderEntries })
  } catch (e) {
    console.log('Cannot update order after its entry update', e)
  }
}

const getProductPrice = async (id, quantity) => {
  try {
    if (id) {
      const product = await strapi.services.product.findOne({ id })
      const { price } = sanitizeEntity(product, { model: strapi.models.product })
      return price * quantity
    }
  } catch (e) {
    console.log('Cannot calculate price for order entry, product cannot be found', e)
  }
}

module.exports = {
  lifecycles: {
    beforeCreate: async (data) => {
      data.price = await getProductPrice(data.product, data.quantity)
    },
    beforeUpdate: async (params, data) => {
      data.price = await getProductPrice(data.product, data.quantity)
    },
    afterCreate: async (result, data) => {
      updateOrder(data.order)
    },
    afterUpdate: async (result, params, data) => {
      updateOrder(data.order)
    }
  }
}
