const { sanitizeEntity } = require('strapi-utils')

module.exports = {
  isInStock: async ctx => {
    const { sku, count } = ctx.params
    try {
      const product = await strapi.services.product.findOne({ 'sku': sku })
      const { stock } = sanitizeEntity(product, { model: strapi.models.product })
      return stock ? `${count <= stock}` : `${false}`
    } catch (e) {
      const errorMessage = `Cannot get product with sku: ${sku}`
      console.log(errorMessage, e)
      return errorMessage
    }
  }
}


