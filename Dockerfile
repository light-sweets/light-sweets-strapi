FROM strapi/base

WORKDIR "/strapi"

COPY ./package.json ./
COPY ./yarn.lock ./

RUN npm install

COPY . .

ENV NODE_ENV staging
ENV ENV_PATH /strapi/db/.env

RUN npm run build

EXPOSE 1337

CMD ["npm", "run", "start"]
